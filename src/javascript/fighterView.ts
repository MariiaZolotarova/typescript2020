import { createElement } from './helpers/domHelper';
import { IFighterInfo } from './interfaces/IFighterInfo';

export const createFighter = (fighter: IFighterInfo, handleClick: any, selectFighter: (event: any, fighter: IFighterInfo) => Promise<void>): HTMLElement => {
  const { name, source } = fighter;
  const nameElement = createName(name);
  const imageElement = createImage(source);
  const checkboxElement = createCheckbox();
  const fighterContainer = createElement({ tagName: 'div', className: 'fighter' });

  fighterContainer.append(imageElement, nameElement, checkboxElement);

  const preventCheckboxClick = (ev: MouseEvent) => ev.stopPropagation();
  const onCheckboxClick = (ev: any) => selectFighter(ev, fighter);
  const onFighterClick = (ev: MouseEvent) => handleClick(ev, fighter);

  fighterContainer.addEventListener('click', onFighterClick, false);
  checkboxElement.addEventListener('change', onCheckboxClick, false);
  checkboxElement.addEventListener('click', preventCheckboxClick, false);

  return fighterContainer;
};

const createName = (name: string): HTMLElement => {
  const nameElement = createElement({ tagName: 'span', className: 'name' });
  nameElement.innerText = name;

  return nameElement;
};

const createImage = (source: string): HTMLElement => {
  const attributes = { src: source };

  return createElement({ tagName: 'img', className: 'fighter-image', attributes });
};

const createCheckbox = (): HTMLElement => {
  const label = createElement({ tagName: 'label', className: 'custom-checkbox' });
  const span = createElement({ tagName: 'span', className: 'checkmark' });
  const attributes = { type: 'checkbox' };
  const checkboxElement = createElement({ tagName: 'input', attributes });

  label.append(checkboxElement, span);
  return label;
};