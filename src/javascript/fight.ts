import { IFighter } from './interfaces/IFighter';
import { random } from './helpers/randomHelper';

export const fight = (firstFighter: IFighter, secondFighter: IFighter): IFighter | null => {
  const copyfirstFighter = { ...firstFighter };
  const copysecondFighter = { ...secondFighter };

  if (copyfirstFighter.attack * 2 <= (copysecondFighter.defense) && copysecondFighter.attack * 2 <= (copyfirstFighter.defense)) {
    return null;
  }

  while (copyfirstFighter.health > 0 && copysecondFighter.health > 0) {
    copysecondFighter.health -= getDamage(copyfirstFighter, copysecondFighter);

    if (copysecondFighter.health > 0) {
      copyfirstFighter.health -= getDamage(copysecondFighter, copyfirstFighter);
    }
  }

  if (copysecondFighter.health <= 0) {
    return copyfirstFighter;
  }

  return copysecondFighter;
};

export const getDamage = (attacker: IFighter, enemy: IFighter): number => {
  const damage = getHitPower(attacker) - getBlockPower(enemy);

  if (damage < 0) {
    return 0;
  }

  return damage;
};

export const getHitPower = (fighter: IFighter): number => {
  const criticalHitChance = random(1, 2);
  return fighter.attack * criticalHitChance;
};

export const getBlockPower = (fighter: IFighter): number => {
  const dodgeChance = random(1, 2);
  return fighter.defense * dodgeChance;
};
