import { createElement } from '../helpers/domHelper';
import { IFighter } from '../interfaces/IFighter';
import { showModal } from './modal';

export const showFighterDetailsModal = (fighter: IFighter): void =>{
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
};

const createFighterDetails = (fighter: IFighter): HTMLElement =>{
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  // show fighter name, attack, defense, health, image

  nameElement.innerHTML = `
  <ul>
  <li>Name: ${name}</li>
  <li>Attack: ${attack}</li>
  <li>Defense: ${defense}</li>
  <li>Health: ${health}</li>
  </ul>
  <img class="fighter-image" src=${source}>
  `;
  fighterDetails.append(nameElement);

  return fighterDetails;
};
