import { createElement } from '../helpers/domHelper';

export const showModal = ({ title, bodyElement }: { title: string, bodyElement: any }): void => {
  const root = getModalContainer();
  const modal = createModal(title, bodyElement);

  root.append(modal);
};

const getModalContainer = (): HTMLElement | null => {
  return document.getElementById('root');
};

const createModal = (title: string, bodyElement: any): HTMLElement => {
  const layer = createElement({ tagName: 'div', className: 'modal-layer' });
  const modalContainer = createElement({ tagName: 'div', className: 'modal-root' });
  const header = createHeader(title);

  modalContainer.append(header, bodyElement);
  layer.append(modalContainer);

  return layer;
};

const createHeader = (title: string): HTMLElement => {
  const headerElement = createElement({ tagName: 'div', className: 'modal-header' });
  const titleElement = createElement({ tagName: 'span' });
  const closeButton = createElement({ tagName: 'div', className: 'close-btn' });

  titleElement.innerText = title;
  closeButton.innerText = '×';
  closeButton.addEventListener('click', hideModal);
  headerElement.append(title, closeButton);

  return headerElement;
};

const hideModal = (_event: any): void => {
  const modal = document.getElementsByClassName('modal-layer')[0];
  modal?.remove();
};
