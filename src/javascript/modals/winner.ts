import { createElement } from '../helpers/domHelper';
import { IFighter } from '../interfaces/IFighter';
import { showModal } from './modal';

export const showWinnerModal = (fighter: IFighter | null): void => {
  const title = 'Winner info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
};

const createFighterDetails = (fighter: IFighter | null): any => {
  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });

  if (fighter === null) {
    nameElement.innerHTML = `Draw!!!`;
  } else {
    const { name, source } = fighter;

    nameElement.innerHTML = `
      Winner: ${name}
      <img class="fighter-image" src=${source}>
      `;
  }

  fighterDetails.append(nameElement);

  return fighterDetails;
};