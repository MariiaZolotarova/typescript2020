import { fight } from './fight';
import { createFighter } from './fighterView';
import { createElement } from './helpers/domHelper';
import { IFighter } from './interfaces/IFighter';
import { IFighterInfo } from './interfaces/IFighterInfo';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { showWinnerModal } from './modals/winner';
import { getFighterDetails } from './services/fightersService';

export const createFighters = (fighters: IFighterInfo[]): HTMLElement => {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
};

const showFighterDetails = async (_event: MouseEvent, fighter: IFighter): Promise<void> => {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
};

const fightersDetailsCache = new Map();

export const getFighterInfo = async (fighterId: string): Promise<IFighter> => {
  const someFighter = fightersDetailsCache.get(fighterId);

  if (someFighter === undefined) {
    const fighterInfo = await getFighterDetails(fighterId)
    fightersDetailsCache.set(fighterId, fighterInfo)
    return fighterInfo;
  }
  else {
    return someFighter;
  }
};

const createFightersSelector = (): (event: any, fighter: IFighterInfo) => Promise<void> => {
  const selectedFighters = new Map();

  return async (event, fighter): Promise<void> => {
    const fullInfo = await getFighterInfo(fighter._id);

    if (event.target.checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const winner = fight(...selectedFighters.values());
      showWinnerModal(winner);
    }
  };
};