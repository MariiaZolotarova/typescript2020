import { callApi } from '../helpers/apiHelper';
import { IFighter } from '../interfaces/IFighter';
import { IFighterInfo } from '../interfaces/IFighterInfo';

export const getFighters = async (): Promise<IFighterInfo[]> => {
  try {
    const endpoint = 'fighters.json';
    return await callApi(endpoint, 'GET');

  } catch (error) {
    console.error(error);
    throw error;
  }
};

export const getFighterDetails = async (id: string): Promise<IFighter> => {
  try {
    const endpoint = `details/fighter/${id}.json`;
    return await callApi(endpoint, 'GET');
  } catch (error) {
    console.error(error);
    throw error;
  }
};
