export interface IFighterInfo {
    _id: string;
    name: string;
    source: string;
}
